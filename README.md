# OpenML dataset: NewspaperChurn

https://www.openml.org/d/44226

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

This dataset includes 15856 records of individuals who are or were subscribers to this newspaper. Datasets contain demographic information like HH Income which stands for the household income, home ownership, dummy for Children, Ethnicity, Year Of Residence, Age range, Language, and Nielsen Prizm; Geographic information like Address, State, City, County, and Zip Code. Also, the delivery period is chosen by the particular subscriber, as well as the weekly charge associated with it. The dataset also included the number of rewards subscribers used, and the source channel he/she been recruited in the first place. Finally, dataset included the information of whether the customer is still our subscriber or not.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/44226) of an [OpenML dataset](https://www.openml.org/d/44226). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/44226/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/44226/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/44226/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

